#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
        vector<int> rotatedVector;
        int         rotation;
    
        void rotateArray(int rot)
        {
            int len = (int)arrVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                rotatedVector.push_back(arrVector[(i+rot) % len]);
            }
        }
    
        void displayRotatedVector()
        {
            int len = (int)rotatedVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<rotatedVector[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
        Engine(vector<int> aV , int rot)
        {
            arrVector = aV;
            rotation  = rot%(int)arrVector.size();
            rotateArray(rotation);
        }
    
        int findPivot()
        {
            int start = 0;
            int end   = (int)rotatedVector.size() - 1;
            while (start < end)
            {
                int mid = (start + end) / 2;
                if(rotatedVector[mid] < rotatedVector[mid-1])
                {
                    return rotatedVector[mid];
                }
                if(rotatedVector[end-1] > rotatedVector[end])
                {
                    return rotatedVector[end];
                }
                if(!rotation)
                {
                    return rotatedVector[start];
                }
                if(rotatedVector[start] <= rotatedVector[mid])
                {
                    start = mid;
                }
                else
                {
                    end = mid - 1;
                }
            }
            return -1;
        }
};

int main(int argc, const char * argv[])
{
    vector<int> arrVector = {3 , 5 , 6 , 10 , 13 , 21 , 28 , 50};
    Engine      e         = Engine(arrVector , 5);
    cout<<e.findPivot()<<endl;
    return 0;
}
